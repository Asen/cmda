﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MemDump
{
    public partial class Form1 : Form
    {
        [DllImport("kernel32.dll")]
        public static extern uint GetLastError();

        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool ReadProcessMemory(
            IntPtr hProcess,
            IntPtr lpBaseAddress,
            [Out] byte[] lpBuffer,
            int dwSize,
            out IntPtr lpNumberOfBytesRead);

        private static SortedSet<int> activePids = new SortedSet<int>();
        private static List<Thread> threads = new List<Thread>();
        private static long lastPos = 0;

        private readonly String DUMP_FOLDER = null;
        private readonly Encoding ENC = Encoding.Unicode;

        private long max_mem_address = Int16.MaxValue;

        private int PID = -1;
        private String HTML_TAG = null;

        public Form1()
        {
            InitializeComponent();

            DUMP_FOLDER = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            DUMP_FOLDER = Path.Combine(DUMP_FOLDER, "html_dump");
            if (!Directory.Exists(DUMP_FOLDER)) Directory.CreateDirectory(DUMP_FOLDER);

            max_mem_address = (long)SystemInfo.getSysInfo().maximumApplicationAddress;          

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var check = groupBox1.Controls.
                OfType<RadioButton>().FirstOrDefault(c => c.Checked);

            HTML_TAG = check==null || check.Text == "full-dump" ? "" : check.Text;
            PID = (int)numericUpDown1.Value;

            foreach (Process p in Process.GetProcesses())
            {
                if (p.ProcessName.ToLower().Trim() == "chrome" && p.Id==PID)
                {
                    Directory.CreateDirectory(Path.Combine(DUMP_FOLDER, p.Id.ToString()));
                    Thread thread = new Thread(Dumper);
                    threads.Add(thread);
                    thread.Start(p.Id);
                    activePids.Add(p.Id);
                }
                informator.Start();
            }
        }

        private void Dumper(object pid)
        {
            int process_id = (int)pid;
            Process p = Process.GetProcessById(process_id);
            IntPtr read = IntPtr.Zero;

            SystemInfo.SYSTEM_INFO SysInfo = SystemInfo.getSysInfo();
            long min_address = (long)SysInfo.minimumApplicationAddress;
            long max_address = (long)SysInfo.maximumApplicationAddress;

            do
            {
                VirtualMemInfo.MEMORY_BASIC_INFORMATION memInfo =  
                    VirtualMemInfo.getMemoryPageInfo(p.Handle, (IntPtr)min_address);
                int memProtect = memInfo.AllocationProtect;
                int memBase = memInfo.BaseAddress;
                if (memInfo.State != 0x1000)
                {
                    min_address += memInfo.RegionSize;
                    continue;
                }

                byte[] buffer = new byte[memInfo.RegionSize];
                bool readRes = ReadProcessMemory(p.Handle, (IntPtr)memBase, buffer, 
                    memInfo.RegionSize, out read);

                uint code = GetLastError();
                if (read.Equals(0)) continue;

                String dumpStr = ENC.GetString(buffer);
                //dumpStr = new String(dumpStr.Where(ch => (int)ch != 0).ToArray());

                String filter = @"[^a-zA-Z0-9а-яА-Я@\r\ns -\<\>\[\]\{\}\(\)\&\?:\|\!\+\=\#'/"+'"'+"]";
                String usefulData = new Regex(filter).Replace(dumpStr.ToString(), "");
                usefulData = new DumpFilter(usefulData.ToString(),false).
                    Filter("<"+HTML_TAG,HTML_TAG+">");
                //String usefulData = Regex.Match(dumpStr, "<div(.*)</div>").ToString();

                if (usefulData.Length>512)
                    File.WriteAllText(
                        Path.Combine(DUMP_FOLDER, process_id.ToString(),
                        CompileFileName(process_id, memBase, memProtect)),
                        usefulData, ENC);

                min_address += memInfo.RegionSize; 
                lastPos = min_address;

            } while (min_address < max_address);

            lastPos = max_address;
            activePids.Remove(process_id);

        }

        private String CompileFileName(int pid, int memBase, int memProtection)
        {
            Dictionary<int, String> memProtections = new Dictionary<int,string>();

            memProtections.Add(0x10, "EXECUTE");
            memProtections.Add(0x20, "EXECUTE-READ");
            memProtections.Add(0x40, "EXECUTE-READWRITE");
            memProtections.Add(0x80, "EXECUTE-WRITECOPY");
            memProtections.Add(0x01, "NOACCESS");
            memProtections.Add(0x02, "READONLY");
            memProtections.Add(0x04, "READWRITE");
            memProtections.Add(0x08, "WRITECOPY");

            return pid + "_" + memBase + "_" + memProtections[memProtection] + ".html";
        }

        private void informator_Tick(object sender, EventArgs e)
        {
            this.Text = lastPos + "";
            progressBar1.Value = (int)(((double)lastPos / max_mem_address) * 100);

            if (lastPos >= max_mem_address) {
                informator.Stop();
                progressBar1.Value = 100;
                MessageBox.Show("Dump saved in "+DUMP_FOLDER);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            foreach (Thread thread in threads)
                thread.Abort();
            //Application.Exit();
        }

    }
}
