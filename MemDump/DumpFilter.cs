﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MemDump
{
    class DumpFilter
    {
        private String dump = String.Empty;
        private Boolean isFull = false;

        public DumpFilter(String _dump, Boolean isFullFilter = false)
        { 
            dump = _dump; 
            isFull = isFullFilter;
        }

        public String Filter(String startStr, String endStr)
        {
            startStr = startStr.ToLower();
            endStr = endStr.ToLower();

            int startPos = dump.ToLower().IndexOf(startStr);
            int endPos = dump.ToLower().LastIndexOf(endStr);
            int endLen = endStr.ToLower().Length;
            String data = (startPos!=-1 && endPos > startPos) ? 
                dump.Substring(startPos, endPos - startPos + endStr.Length) : "";

            return true ? _FilterScripts(data) : data;

        }

        private String _FilterScripts(String original, int startIdx = 0)
        {
            String scrStart = "<script";
            String scrEnd = "script>";

            return original;

            int startPos = original.IndexOf(scrStart, startIdx);
            if(startPos>-1)
            {
                int endPos = original.IndexOf(scrEnd, startPos);
                return endPos>startPos ? 
                    _FilterScripts(original.Remove(startPos, endPos-startPos+scrEnd.Length), startPos+1) : 
                    original;
            }

            return original;

        }

    }
}
