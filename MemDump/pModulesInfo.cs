﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MemDump
{
    class pModulesInfo
    {

        public static SortedDictionary<IntPtr, ProcessModule> getModulesInfo(int PID)
        {
            SortedDictionary<IntPtr, ProcessModule> pModules =
               new SortedDictionary<IntPtr, ProcessModule>(new pModuleComp());
            foreach (Process p in Process.GetProcesses())
                if (p.Id == PID)
                    foreach (ProcessModule module in p.Modules)
                        pModules.Add(module.BaseAddress, module);

            return pModules;
        }

    }

    class pModuleComp : IComparer<IntPtr>
    {
        public int Compare(IntPtr x, IntPtr y)
        {
            int a = (int)x;
            int b = (int)y;

            if (a == b) return 0;
            return a < b ? -1 : 1;
        }
    }

}
